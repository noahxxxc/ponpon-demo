#build image 指令： docker build -t ponpon .
FROM node:12-alpine

#設定在docker image裡面的工作目錄
WORKDIR /app
#從本機的路徑(從執行docker build 的路徑開始計算)複製檔案到docker image裡面
#等於複製整個ponpon專案下的檔案到docker image裡面的/app(工作目錄 - WORKDIR)
COPY . .
#npm ci 比 npm install快,給ci/cd安裝node_module用的
RUN npm ci
#container執行起來後，就執行這個指令
ENTRYPOINT ["npm", "run", "serve"]