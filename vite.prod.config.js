import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  base: '/ponpon-demo/',
  plugins: [vue()],
  server: {
    host: true
  },
  resolve: {
    alias: {
      '/@': path.resolve(__dirname, 'src'),
      '/Styles': path.resolve(__dirname, 'src/assets/styles'),
      '/Services': path.resolve(__dirname, 'src/services'),
    },
  },
  build: {
    emptyOutDir: true,
    assetsDir: 'assets/',
    chunkSizeWarningLimit: 600,
    rollupOptions: {
      output: {
        manualChunks: {
          firebaseVendor: ['firebase'],
          vendor: ['vue', 'vue-router', 'vuex']
        }
      }
    }
  }
})
