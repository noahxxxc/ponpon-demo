import { createApp } from 'vue'
import router from './app.router.js'
import store from './app.store.js'

import App from './App.vue'

import './assets/styles/main.scss'

router.beforeEach((to, from, next) => {
  store.dispatch('toggleNavigation', false)
  if(to.meta.requiresAuth) {
    if(!store.state.user){
      alert('請先登入')
      next({name: 'signIn', query: {go: 'edit-daily-log'}})
    }
    next()
  }
  next()
})


createApp(App)
  .use(router)
  .use(store)
  .mount('#app')
