import { createStore } from 'vuex'
import { auth, db, firebase} from '/Services/FirebaseApp'

const state = {
  user: '',
  isShowNavigation: false,
  dailyLogInfo: {
    createDate: null,
    lastEditTime: '',
    dayFeed: '',
    dayPoo: '',
    dayWalking: '',
    nightFeed: '',
    nightPoo: '',
    nightWalking: '',
    dayMemo: '',
    nightMemo: ''
  },

  isGetDailyLogInfoLoading: false,

  signUpResult: null,
  signInResult: null,

  isSignInState: false,
  signInErrorMessage: '',
}

const getters = {
  dateString() {
    return (theDate = new Date()) => {
      return {
        year: `${theDate.getFullYear()}`,
        month: ((theDate.getMonth() + 1) < 10 ? '0' : '') + (theDate.getMonth() + 1),
        date: (theDate.getDate() < 10 ? '0' : '') + theDate.getDate(),
        hours: (theDate.getHours() < 10 ? '0' : '') + theDate.getHours(),
        minutes: (theDate.getMinutes() < 10 ? '0' : '') + theDate.getMinutes(),
      }
    }
  },
  dateTextData(state, getters) {
    return (theDate = new Date()) => {
      let dateString = getters.dateString(theDate)
      return {
        text: `${dateString.year}/${dateString.month}/${dateString.date}`,
        date: dateString.year + dateString.month + dateString.date,
        timeText: `${dateString.hours}:${dateString.minutes}`,
        timestamp: Date.parse(theDate)
      }
    }
  }
}

const mutations = {
  SET_USER(state, data) {
    state.user = data
  },
  SET_IS_SHOW_NAVIGATION(state, data) {
    state.isShowNavigation = data
  },
  SET_DAILY_LOG_INFO(state, data) {
    state.dailyLogInfo = data
  },
  IS_GET_DAILY_LOG_INFO_LOADING(state, boolean) {
    state.isGetDailyLogInfoLoading = boolean
  },
  SIGN_UP_RESULT(state, boolean) {
    state.signUpResult = boolean
  },
  SIGN_IN_RESULT(state, boolean) {
    state.signInResult = boolean
  },
  UPDATE_SIGN_IN_STATE(state, boolean) {
    state.isSignInState = boolean
  },
  UPDATE_SIGN_IN_ERROR_MESSAGE(state, data) {
    state.signInErrorMessage = data
  }
}

const actions = {
  toggleNavigation({commit}, boolean) {
    commit('SET_IS_SHOW_NAVIGATION', boolean)
  },

  getDailyLogInfo({commit, dispatch}, data) {
    commit('IS_GET_DAILY_LOG_INFO_LOADING', true)
    db.collection('dailyLog')
      .doc(data)
      .get()
      .then(data => {
        commit('IS_GET_DAILY_LOG_INFO_LOADING', false)
        if(data.exists) {
          commit('SET_DAILY_LOG_INFO', data.data())
        }
      })
      .catch(err => {
        commit('IS_GET_DAILY_LOG_INFO_LOADING', false)
      })
  },

  setDailyLogInfo({commit}, data) {
    db.collection('dailyLog')
      .doc(data.date)
      .set(data.data)
      .then(() => {
        commit('SET_DAILY_LOG_INFO', data.data)
      })
      .catch(() => {})
  },

  isGetDailyLogInfoLoading({commit}, boolean) {
    commit('IS_GET_DAILY_LOG_INFO_LOADING', boolean)
  },

  signUp({commit}, data) {
    auth.createUserWithEmailAndPassword(data.account, data.password)
        .then(userCredential => {
          commit('SET_USER', userCredential.user.email)
          commit('SIGN_UP_RESULT', true)
          commit('UPDATE_SIGN_IN_STATE', true)
        })
        .catch(error => {
          commit('SIGN_UP_RESULT', false)
          commit('UPDATE_SIGN_IN_STATE', false)
        });
  },

  signIn({commit}, data) {
    commit('SIGN_IN_RESULT', null)
    auth.signInWithEmailAndPassword(data.account, data.password)
        .then(userCredential => {
          commit('SET_USER', userCredential.user.email)
          commit('SIGN_IN_RESULT', true)
          commit('UPDATE_SIGN_IN_STATE', true)
        })
        .catch(error => {
          commit('UPDATE_SIGN_IN_ERROR_MESSAGE', '帳號或密碼有誤，請')
          commit('SIGN_IN_RESULT', false)
          commit('UPDATE_SIGN_IN_STATE', false)
        });
  },

  signOut({commit}, data) {
    auth.signOut()
        .then(() => {
          commit('SET_USER', '')
          commit('UPDATE_SIGN_IN_STATE', false)
        })
        .catch((error) => {
          commit('UPDATE_SIGN_IN_STATE', true)
        })
  },

  fetchOnAuthState({commit}) {
    auth.onAuthStateChanged(user => {
      if(user) {
        commit('UPDATE_SIGN_IN_STATE', true)
        commit('SET_USER',  user.email)
      } else {
        commit('UPDATE_SIGN_IN_STATE', false)
        commit('SET_USER', '')
      }
    });
  },

  // signInWithFacebook({commit}) {
  //   auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
  //       .then(result => {
  //         alert('has result')
  //       })
  //       .catch(error => {
  //       })
  // },

  // currentUser({commit}) {
  //   const user = firebase.auth().currentUser;
  //   if (user !== null) {

  //      console.log('has user', user)
  //     const uid = user.uid;
  //   }
  //   else {
  //     console.log('no user')
  //   }
  // }
}

export default createStore({
  state,
  getters,
  mutations,
  actions
})


