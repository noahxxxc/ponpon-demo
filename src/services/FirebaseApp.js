import firebase from 'firebase/app'
import "firebase/auth"
import 'firebase/firestore'


const firebaseConfig = {
  apiKey: "AIzaSyAD7eOFIgltsfL1Om86UER72uQWhsiFCBw",
  authDomain: "ponpon-cd433.firebaseapp.com",
  projectId: "ponpon-cd433",
  storageBucket: "ponpon-cd433.appspot.com",
  messagingSenderId: "829060742404",
  appId: "1:829060742404:web:20110334018c07e925a451",
  measurementId: "G-5977ENF2TL"
}

const firebaseApp = firebase.initializeApp(firebaseConfig)
const auth = firebaseApp.auth()
const db = firebaseApp.firestore()


export {
  auth,
  db,
  firebase
}