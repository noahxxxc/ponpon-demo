const feedStatus = {
  'done': '吃光光',
  'nibble': '吃一點點',
  'none': '沒吃'
}


const pooStatus = {
  'normal': '正常',
  'none': '沒有便便',
  'constipation': '便秘',
  'wet': '偏濕',
  'diarrhea': '拉肚子'
}

const walkingStatus = {
  'done': '散步了',
  'none': '沒有出門',
}


export {
  feedStatus,
  pooStatus,
  walkingStatus
}