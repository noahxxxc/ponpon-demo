import { createRouter, createWebHashHistory } from 'vue-router'

import Home from './pages/Home.vue'
import EditDailyLog from './pages/EditDailyLog.vue'
import SignUp from './pages/SignUp.vue'
import SignIn from './pages/SignIn.vue'

const routers = [
  { path: '/index.html',
    name: 'home',
    component: Home
  },
  { path: '/',
    name: 'home',
    component: Home
  },
  { path: '/edit-daily-log',
    name: 'editDailyLog',
    component: EditDailyLog,
    meta: { 
      requiresAuth: true
    }
  },
  { path: '/signup',
    name: 'signUp',
    component: SignUp
  },
  { path: '/signin',
    name: 'signIn',
    component: SignIn
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes: routers,
})




export default router